const path = require('path');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
//const devMode = process.env.NODE_ENV !== 'production';

// if (!devMode) {
//     // enable in production only
//     plugins.push(new MiniCssExtractPlugin());
// }

module.exports = {
    entry: './src/js/index.js',
    mode: 'development',
    output: {
        filename: 'main.js',
        path: path.resolve(__dirname, 'dist'),
    },
    watch: true,
    watchOptions: {
        aggregateTimeout: 200,
        poll: 1000,
    },
    plugins: [

        new MiniCssExtractPlugin({
            filename: "main.css"
        })

    ],
    module: {
        rules: [
            {
                test: /\.(sa|sc|c)ss$/,
                use: [
                    MiniCssExtractPlugin.loader,
                    'css-loader',
                    'sass-loader',
                ]
            },
            {
                test: /\.(jpg|jpeg|png|woff|woff2|eot|ttf|svg)$/,
                loader: 'url-loader'
            },
            // {
            //     test: /\.s[ac]ss$/i,
            //     loader: MiniCssExtractPlugin.extract(['css-loader', 'sass-loader'])
            // },
        ],
    },
};