import _ from "lodash";

export function init() {
    console.log('header.js');


    if($('.header-nav-container').length > 0){
	$(window).on('scroll', _.debounce(function() {
        
        console.log(window.pageYOffset +' > '+$('.header-nav-container').offset().top);
        if(window.pageYOffset > 63) {
            $('body').addClass('is-scrolled');
        } else {
            $('body').removeClass('is-scrolled');
        }
    
    }, 300));

   
    }
    $('.js-nav-menu-trigger').on('click', function(){
        $('.js-nav-menu').toggleClass('open');
    })

}